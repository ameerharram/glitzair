<?php

use App\Http\Controllers\API\UserPermissionController;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });

Route::get('/admin-users', [UserPermissionController::class, 'getUsers']);
Route::get('/permissions', [UserPermissionController::class, 'getPermissions']);
Route::post('/assign-permissions', [UserPermissionController::class, 'assignPermissions']);
