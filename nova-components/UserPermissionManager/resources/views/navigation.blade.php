<router-link
    tag="h3"
    :to="{name: 'user-permission-manager'}"
    class="sidebar-link cursor-pointer flex items-center font-normal dim text-white text-base no-underline">
    <svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="var(--sidebar-icon)" width="20" height="20">
        <path d="M12 1.5L3 5.25v5.5c0 5.75 4.13 10.87 9 12.25 4.87-1.38 9-6.5 9-12.25v-5.5L12 1.5zM12 3l7 3.25v4.75c0 4.88-3.5 9.38-7 10.63-3.5-1.25-7-5.75-7-10.63V6.25L12 3zM11 10v3h2v-3h-2zm0 5v2h2v-2h-2z"/>
    </svg>
    <span class="sidebar-label">
        Permissions
    </span>
</router-link>
