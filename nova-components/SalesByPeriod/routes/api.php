<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Card API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your card. These routes
| are loaded by the ServiceProvider of your card. You're free to add
| as many additional routes to this file as your card may require.
|
*/

Route::get('/get-sales-data',  [App\Http\Controllers\API\StationController::class, 'getAdminGrapSalesData'])->name('admin.sales.data');
Route::get('/get-sales-yearly-data',  [App\Http\Controllers\API\StationController::class, 'getAdminGrapSalesYearlyData'])->name('admin.sales.yearly.data');
Route::get('/get-location-performance-data',  [App\Http\Controllers\API\StationController::class, 'getLocationPerformance'])->name('admin.location.performance.data');

