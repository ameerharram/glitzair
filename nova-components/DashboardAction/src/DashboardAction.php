<?php

namespace Ameer\DashboardAction;

use App\Models\Station;
use App\Models\User;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Card;

class DashboardAction extends Card
{
    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = '1/3';
   public $someData = 'Hello from your card!';


  /**
     * Get the component name for the element.
     *
     * @return string
     */
    public function component()
    {
      return 'dashboard-action';
    }
}
