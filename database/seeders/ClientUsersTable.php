<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Station;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ClientUsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sations = Station::all();
        foreach ($sations as $sation)
        {
            if($sation->owner_id != 0 && $sation->owner_id != null){
                $uuid = Str::uuid();
                $user = User::find($sation->owner_id);
                $user->uuid = $uuid;
                $user->role_id = User::ROLE_CLIENT;
                $user->client_uuid = $uuid;
                $user->save();
                $user->assignRole(User::ROLE_CLIENT);
            }
        }
    }
}
