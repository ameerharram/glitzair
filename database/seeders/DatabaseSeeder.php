<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
              //CreateRoles::class,
              //CreateUsers::class,
              //UpdatePasswordForTest::class,
              //ClientUsersTable::class,
              //MachineTypeColPopulate::class
        ]);
    }
}
