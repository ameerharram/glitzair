<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = [
            [
                "id" => 1,
                "name" => "Glitz Air",
                "email" => "ishan@glitzair.com",
                "uuid" => Str::uuid(),
                "client_uuid" => Str::uuid(),
                "role_id" => User::ROLE_OWNER,
                "password" => Hash::make("GlitzAir2022!"),
            ]
        ];
        User::upsert($owner,['id'],['name','email','password','role_id']);
        User::find(User::OWNER_ID)->assignRole(User::ROLE_OWNER);

        //Client Added
        $defaultUser = User::where('email','default@glitzair.com')->first();
        if(!$defaultUser){

          $uuid = Str::orderedUuid();
          $defaultUser = User::create([
                                      "name" => "default",
                                      "email" => "default@glitzair.com",
                                      "uuid" => $uuid,
                                      "client_uuid" => $uuid,
                                      "role_id" => User::ROLE_CLIENT,
                                      "password" => Hash::make("GlitzAir2022!"),
                                    ]);

          $defaultUser->assignRole(User::ROLE_CLIENT);
        }

    }
}
