<?php

namespace Database\Seeders;

use App\Models\Machine;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MachineTypeColPopulate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Initial Records are of seedlive.
        Machine::where('machine_type',0)
               ->update([
                   'machine_type' => Machine::TYPE_SEEDLIVE
               ]);
    }
}
