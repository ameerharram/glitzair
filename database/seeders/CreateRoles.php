<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class CreateRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            [
                'id' => User::ROLE_OWNER,
                'name' => 'owner',
                'guard_name' => 'web'
            ],
            [
                'id' => User::ROLE_ADMIN,
                'name' => 'admin',
                'guard_name' => 'web'
            ],
            [
                'id' => User::ROLE_CLIENT,
                'name' => 'client',
                'guard_name' => 'web'
            ],
            [
                'id' => User::ROLE_SUB_CLIENT,
                'name' => 'sub-client',
                'guard_name' => 'web'
            ],

        ];
        Role::upsert($data,['id'],['name','guard_name']);
    }
}
