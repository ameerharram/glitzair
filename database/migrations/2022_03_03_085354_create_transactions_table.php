<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('machine_id')->nullable();
            $table->smallInteger('machine_type')->nullable();
            $table->unsignedInteger('location_id')->nullable();
            $table->string('device_number');
            $table->unsignedBigInteger('ref_number');
            $table->dateTime('transaction_date');
            $table->smallInteger('transaction_type');
            $table->float('amount');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
