<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('machines', function (Blueprint $table) {
            $table->string('cash')->nullable()->change();
            $table->string('cash_and_credit')->nullable()->change();
            $table->unsignedSmallInteger('status')->default(1)->change();
            $table->unsignedInteger('created_by')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('machines', function (Blueprint $table) {
            $table->string('cash')->change();
            $table->string('cash_and_credit')->change();
            $table->unsignedSmallInteger('status')->change();
            $table->unsignedInteger('created_by')->change();
        });
    }
};
