<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            
            // Indexes to optimize queries
            $table->index('device_number'); 
            $table->index(['transaction_date']); 

            $table->index(['device_number', 'transaction_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropIndex(['device_number']);
        $table->dropIndex(['transaction_date']); 
        $table->dropIndex(['device_number', 'transaction_date']); 
    }
};
