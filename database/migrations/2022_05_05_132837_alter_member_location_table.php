<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('member_locations', function (Blueprint $table) {
        $table->unsignedInteger('user_id')->nullable()->after('station_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('machines', function (Blueprint $table) {
        $table->unsignedInteger('user_id')->change();
      });
    }
};
