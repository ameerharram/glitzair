<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quarter_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('location_id');
            $table->string('year');
            $table->tinyInteger('quarter_no');
            $table->date('quarter_start_date');
            $table->date('quarter_end_date');
            $table->float('commission');
            $table->float('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quarter_log');
    }
};
