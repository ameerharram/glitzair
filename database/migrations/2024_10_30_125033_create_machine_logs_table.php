<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_logs', function (Blueprint $table) {
            $table->id(); // Primary key
            $table->integer('machine_id')->nullable(); 
            $table->string('machine_number')->nullable(); 
            $table->string('type')->nullable(); 
            $table->string('device_number')->nullable(); 
            $table->timestamp('last_transaction_at')->nullable(); // Log creation timestamp
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_logs');
    }
};
