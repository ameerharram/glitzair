<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins_collection', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('location_id');
            $table->string('collection_id')->unique();
            $table->string('technician_id')->nullable();
            $table->float('coins_collected');
            $table->dateTime('visit_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins_collection');
    }
};
