<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stations', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->nullable()->change();
            $table->unsignedSmallInteger('status')->default(1)->change();
            $table->unsignedInteger('created_by')->nullable()->change();
            $table->string('address_2')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stations', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->change();
            $table->unsignedSmallInteger('status')->change();
            $table->unsignedInteger('created_by')->change();
            $table->string('address_2')->change();
        });
    }
};
