<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stations', function (Blueprint $table) {
          $table->float('pre_quarter_com')->nullable()->after('commission_percent');
          $table->float('this_quarter_com')->nullable()->after('pre_quarter_com');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stations', function (Blueprint $table) {
            $table->dropColumn('pre_quarter_com');
            $table->dropColumn('this_quarter_com');
        });
    }
};
