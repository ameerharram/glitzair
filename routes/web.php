<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Auth
 */
Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('client.logout');

Route::get('{uuid}/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('{uuid}/location/{id}/dashboard', [App\Http\Controllers\HomeController::class, 'location'])->name('location');

Route::get('{uuid}/teams', [App\Http\Controllers\HomeController::class, 'myTeams'])->name('teams.list');
Route::get('{uuid}/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');

Route::get('users/inviation/{invite_code}', [App\Http\Controllers\HomeController::class, 'signupViaInvite'])->name('invite.signup');

Route::get('download-monthly-report/{current_date}', [App\Http\Controllers\HomeController::class, 'downloadMonthlyReport'])->name('download.monthly.report');


Route::get('check403', [App\Http\Controllers\HomeController::class, 'check403'])->name('check403');
Route::get('sheetdb', [App\Http\Controllers\HomeController::class, 'sheetDb'])->name('sheetDb');




