<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Ajax Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Locations
 */
Route::get('{client_uuid}/locations', [App\Http\Controllers\API\StationController::class, 'index'])->name('locations.index');

Route::get('{client_uuid}/locations/{location_id}', [App\Http\Controllers\API\StationController::class, 'show'])->name('locations.show');

Route::post('{client_uuid}/locations/{location_id}/update', [App\Http\Controllers\API\StationController::class, 'update'])->name('location.update');

Route::get('{client_uuid}/locations/{location_id}/stats', [App\Http\Controllers\API\StationController::class, 'locationStats'])->name('locations.stats');

Route::get('{client_uuid}/locations/{location_id}/sales-details',  [App\Http\Controllers\API\StationController::class, 'locationSalesDetail'])->name('locations.sales.detail');

Route::get('{client_uuid}/locations/{location_id}/get-sales-report',  [App\Http\Controllers\API\StationController::class, 'getSalesReportFile'])->name('locations.sales.report');

Route::post('{client_uuid}/locations/{location_id}/support',  [App\Http\Controllers\API\StationController::class, 'sendSupport'])->name('locations.support');

Route::get('{client_uuid}/locations/{location_id}/get-graph-data',  [App\Http\Controllers\API\StationController::class, 'graphData'])->name('locations.graph.data');

Route::post('{client_uuid}/users/send-invite',  [App\Http\Controllers\API\UserController::class, 'sendInvite'])->name('users.invite.send');

Route::post('{client_uuid}/member/{member_id}/locations',  [App\Http\Controllers\API\UserController::class, 'updateMemberLocation'])->name('update.member.locations');

Route::post('users/accept-invite/{invite_code}',  [App\Http\Controllers\API\UserController::class, 'acceptInvite'])->name('users.invite.accept');

Route::get('{client_uuid}/team',  [App\Http\Controllers\API\UserController::class, 'getTeam'])->name('users.team.index');

Route::post('{client_uuid}/team/{team_id}/delete',  [App\Http\Controllers\API\UserController::class, 'deleteMember'])->name('users.team.delete');

Route::post('{uuid}/profile', [App\Http\Controllers\API\UserController::class, 'update'])->name('profile.update');

