@extends('layouts.app')

@section('content')
  <locations-list client-uuid="{{ request()->route()->parameter('uuid') }}"></locations-list>
@endsection
