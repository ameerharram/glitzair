@extends('layouts.app')

@section('content')
  <teams client-uuid="{{ request()->route()->parameter('uuid') }}"></teams>
@endsection
