@extends('layouts.open_app')

@section('content')
  <invite-signup invite-code="{{ request()->route()->parameter('invite_code') }}" ></invite-signup>
@endsection
