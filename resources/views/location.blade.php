@extends('layouts.app')

@section('content')
  <div class="container">
    <location-dashboard client-uuid="{{ request()->route()->parameter('uuid') }}" location-id="{{ request()->route()->parameter('id') }}"></location-dashboard>
  </div>
@endsection
