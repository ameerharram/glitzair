<p class="mt-8 text-center text-xs text-80">
    <a href="https://glitzair.com/" class="text-primary dim no-underline">Glitz Air</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} GlitzAir. All Rights Reserved.
</p>
