@extends('layouts.app')

@section('content')
  <profile client-uuid="{{ request()->route()->parameter('uuid') }}" user="{{ auth()->user() }}"></profile>
@endsection
