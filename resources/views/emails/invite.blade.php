@component('mail::message')
  Hi,

  Your have been invited by <b>{{$invited_by}}</b>!

  Please click on the link below to accept invite

  @component('mail::button', ['url' => $invite_url])
    Accept Invite
  @endcomponent

  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
