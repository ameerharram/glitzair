@component('mail::message')
  Hi,
  Please click on the button below to download the report.

  @component('mail::button', ['url' => $url])
   Download Report
  @endcomponent

  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
