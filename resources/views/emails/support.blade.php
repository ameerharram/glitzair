@component('mail::message')
  <p><b>User Name:</b> {{$user_name}}</p>
  <p><b>Phone:</b> {{$location_phone}}</p>
  <p><b>Location Name:</b> {{$location_name}}</p>
  <p><b>Reply to:</b> {{$user_email}}</p>
  <p><b>Subject:</b> {{$subject}}</p>
  <p><b>Message:</b> {{$message}}</p>
  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
