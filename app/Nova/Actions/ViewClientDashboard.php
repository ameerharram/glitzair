<?php

namespace App\Nova\Actions;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class ViewClientDashboard extends Action
{
    use InteractsWithQueue, Queueable;
    public $name = 'dashboard';
    public $withoutConfirmation = true;
    public $showOnTableRow = true;
    public $onlyOnIndex = true;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
      $location = $models->first();
      if( !$location->owner_id  ){
        return Action::danger('Please attach any client first.');
      }

      $user = User::find( $location->owner_id );

      if( !$user->client_uuid ){
        return Action::danger('This client is incorrect.');
      }

      return Action::openInNewTab( route('location',['uuid' => $user->client_uuid, 'id' => $location->id]) );
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
