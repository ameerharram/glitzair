<?php

namespace App\Nova;

use App\Models\Station;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Machine extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Machine::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','machine_type','credit','cash_and_credit'
    ];

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {

        $user = $request->user();
        if($user->hasAnyRole('owner','admin'))
        {
          if($request->search)
          {
            //$query = $query->leftJoin('stations', 'stations.id', '=', 'machines.station_id');
            //$query = $query->orWhere('stations.name','like','%'.$request->search.'%');
          }
          return $query;
        }
        else
        {
            return $query->whereHas('station',function ($query) use ($user){
                $query->where('owner_id',$user->id);
            });
        }
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable()
              ->rules(
                [
                  'required','string'
                ]
              ),
            Select::make('Type','machine_type')->options([
              \App\Models\Machine::TYPE_SEEDLIVE => 'Seedlive',
              \App\Models\Machine::TYPE_NAYAX => 'Nayax',
            ])->displayUsingLabels()->sortable()
              ->rules(
                [
                  'required','string'
                ]
              ),
           BelongsTo::make('Location','station','App\Nova\Location')
            ->display(function ($location){
              return "$location->name" ;
            })
            ->sortable()
            ->hideWhenCreating()
            ->hideWhenUpdating(),
            Text::make('Status','status',function (){
              return $this->status == 1 ? 'active' : 'inactive';
            })->hideWhenUpdating()
              ->hideWhenCreating()
              ->sortable(),
            Text::make('Credit')->sortable()
              ->rules(
                [
                  'required_without:cash_and_credit',
                  'different:cash_and_credit',
                  'nullable',
                  function ($attribute, $value, $fail) {
                    $resourceId = \request()->route()->parameter('resourceId');
                    $machine = Machine::whereNot('id',$resourceId)
                      ->where(function ($query) use ($value){
                        $query->where('credit',$value)->orWhere('cash_and_credit',$value);
                      })
                      ->first();
                    if ( $machine ) {
                      $location = Station::where('id',$machine->station_id)->first();

                      if($location)
                      {
                        $fail('This machine '.$value.' is already bind with location "'. $location->name.'". Please unbind machine from this location id '.$location->id .'.' );
                      }else
                      {
                        $fail('This '.$value.' is already existed with machine name "'.$machine->name.'". Please unbind machine number from from this machine id '. $machine->id.'.');
                      }

                    }
                  },
                ]
              ),
            Text::make('Credit & Cash','cash_and_credit')->sortable()
              ->rules(
                [
                  'required_without:credit',
                  'different:credit',
                  'nullable',
                  function ($attribute, $value, $fail) {
                    $resourceId = \request()->route()->parameter('resourceId');
                    $machine = Machine::whereNot('id',$resourceId)
                      ->where(function ($query) use ($value){
                        $query->where('credit',$value)->orWhere('cash_and_credit',$value);
                      })
                      ->first();
                    if ( $machine ) {
                      $location = Station::where('id',$machine->station_id)->first();

                      if($location)
                      {
                        $fail('This machine '.$value.' is already bind with location "'. $location->name.'". Please unbind machine from this location id '.$location->id .'.' );
                      }else
                      {
                        $fail('This '.$value.' is already existed with machine name "'.$machine->name.'". Please unbind machine number from from this machine id '. $machine->id.'.');
                      }

                    }
                  },
                ],

              ),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [ ];
    }
}
