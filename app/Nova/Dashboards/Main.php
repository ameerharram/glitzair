<?php

namespace App\Nova\Dashboards;

use Ameer\SalesByPeriod\SalesByPeriod;
use Laravel\Nova\Dashboard;

class Main extends Dashboard
{

    public $icon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-6 h-6"><path d="M3 9a1 1 0 011 1v6h12V9a1 1 0 112 0v7a1 1 0 01-1 1H4a1 1 0 01-1-1V10a1 1 0 011-1z"/></svg>';


    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label()
    {
        return 'Dashboard';
    }

    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            new SalesByPeriod()
        ];
    }

    /**
     * Get the URI key for the dashboard.
     *
     * @return string
     */
    public static function uriKey()
    {
        return 'v1';
    }
}
