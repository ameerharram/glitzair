<?php

namespace App\Nova;

use Ameer\MachineReport\MachineReport;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Date;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\Badge;

class Logs extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MachineLog::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'device_number';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
      'device_number','last_transaction_at'
    ];

     /**
     * The visual style used for the table. Available options are 'tight' and 'default'.
     *
     * @var string
     */
    public static $tableStyle = 'tight';

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
      
      $query->select(
        'machine_logs.id',
        'machine_logs.device_number',
        'l.name as location_name',
          DB::raw("
                  CASE
                      WHEN last_transaction_at IS NULL THEN 0
                      WHEN machine_number IS NULL THEN 2
                      WHEN TIMESTAMPDIFF(HOUR, last_transaction_at, NOW()) >= 48 THEN 1
                      ELSE 3
                  END AS status,
                  COALESCE(machine_logs.device_number, machine_logs.machine_number) as device_number
                "),
          'last_transaction_at'
        )
        ->leftJoin('stations as l', 'machine_logs.location_id', '=', 'l.id');
        return $query->where(function ($query) {
            $query->whereNull('last_transaction_at')
                  ->orWhere(DB::raw('TIMESTAMPDIFF(HOUR, last_transaction_at, NOW())'), '>=', 48)
                  ->orwhereNull('machine_number');
        })->orderBy('last_transaction_at');
      
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
          Text::make('device_number')->sortable(),
          Text::make('location_name')->sortable(),
          
           //Text::make('status')->sortable(),
           Badge::make('Status',function (){
              
              if($this->status == 0){
                return 'Never Received';    
              }elseif($this->status == 1){
                return 'Inactive - 48+ Hours';
              }
              return 'Not Linked';

            })->map([
              'Never Received' => 'danger',
              'Inactive - 48+ Hours' => 'warning',
              'Not Linked' => 'danger'
             
            ])->sortable(),
          Text::make('last_transaction_at')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
          new MachineReport(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
      return [
          new \App\Nova\Filters\StatusFilter,
      ];
    }  

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
          
        ];
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToView(Request $request)
    {
        return false;
    }
}
