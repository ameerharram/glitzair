<?php

namespace App\Nova;

use App\Nova\Actions\ViewClientDashboard;
use App\Nova\Filters\EndDate;
use App\Nova\Filters\StartDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Collection extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Station::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','name'
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['sales','coins'];

    /**
     * The visual style used for the table. Available options are 'tight' and 'default'.
     *
     * @var string
     */
    public static $tableStyle = 'tight';

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
      $start_date = now()->startOfMonth()->toDateString();
      $end_date = now()->endOfMonth()->toDateString();
      if($request->attributes->get('start_date')){
        $start_date = $request->attributes->get('start_date');
      }
      if($request->attributes->get('end_date')){
        $end_date = $request->attributes->get('end_date');
      }

//      $query->selectRaw(
//        'stations.id,stations.name,(select sum(coins_collection.coins_collected) from coins_collection where coins_collection.location_id = stations.id) as coins_collected,
//         (select sum(s.cash+s.cash_off_hours) from sales s where s.station_id = stations.id ) as recorded_coins'
//      );

      $query->select(
        'stations.id','stations.name',
        DB::raw("(select sum(c.coins_collected) from coins_collection c where c.location_id = stations.id and (c.visit_date between '{$start_date}' AND '{$end_date}') ) as coins_collected"),
        DB::raw("(select sum(s.cash+s.cash_off_hours) from sales s where s.station_id = stations.id and ( s.item_date between '{$start_date}' and '{$end_date}')) as recorded_coins"),
      );
      return $query;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Location Name','name')
                ->rules(
                    [
                        'required','string'
                    ]
                )
                ->required()
                ->sortable(),
            Text::make('coins_collected')
                 ->sortable(),
            Text::make('recorded_coins')
              ->sortable(),
            Badge::make('Status',function (){
              if($this->coins_collected == $this->recorded_coins){
                return 'equal';
              }elseif ($this->coins_collected < $this->recorded_coins)
              {
                return 'lesser';
              }elseif ($this->coins_collected > $this->recorded_coins)
              {
                return 'greater';
              }else
              {
                return 'default';
              }
            })->map([
              'lesser' => 'danger',
              'equal' => 'success',
              'greater' => 'warning',
              'default' => 'info',
            ])->sortable(),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
         new StartDate(),
         new EndDate(),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
          //new ViewClientDashboard
        ];
    }

}
