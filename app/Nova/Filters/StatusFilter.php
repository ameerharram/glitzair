<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use Illuminate\Support\Facades\DB;

class StatusFilter extends Filter
{
    public $name = 'Status';

    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where(function ($query) use ($value) {
            if ($value == 0) {
                $query->whereNull('last_transaction_at')
                      ->whereNotNull('machine_number');
            } elseif ($value == 1) {
                $query->whereRaw('TIMESTAMPDIFF(HOUR, last_transaction_at, NOW()) >= 48')
                      ->whereNotNull('machine_number');
            } elseif ($value == 2) {
                $query->whereNull('machine_number');
            }
        });
    }
    

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Never Received' => 0,
            'Inactive - 48+ Hours' => 1,
            'Not Linked' => 2,
        ];
    }
}
