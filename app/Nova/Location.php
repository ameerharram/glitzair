<?php

namespace App\Nova;

use Ameer\CommissionReport\CommissionReport;
use Ameer\DashboardAction\DashboardAction;
use App\Models\User;
use App\Nova\Actions\ViewClientDashboard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Location extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Station::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','name','address_1','route_no'
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['owner','sales'];

    /**
     * The visual style used for the table. Available options are 'tight' and 'default'.
     *
     * @var string
     */
    public static $tableStyle = 'tight';

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        $user = $request->user();
        if($user->hasAnyRole('owner','admin'))
        {
            return $query;
        }
        else{

            return $query->where('owner_id',$user->id);
        }
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $role = auth()->user()->role_id;
        $canEditTime = false;
        if($role == User::ROLE_OWNER || auth()->user()->can('sale')){
            $canEditTime = true;
        }

        $fields =  [
            ID::make(__('ID'), 'id')->sortable()->hideFromIndex(),
            Text::make('client_uuid','client_uuid',function (){
              $user = User::find( $this->owner_id );
              if( !$user->client_uuid ){
                return null;
              }
              return $user->client_uuid;
            })->onlyOnDetail(),

            BelongsTo::make('Owner','owner','App\Nova\User')
                ->display(function ($user){
                    return "$user->name - $user->email" ;
                })
                ->nullable()
                ->rules([
                    'required','exists:users,id'
                ])
                ->default(env('DEFAULT_CLIENT_ID'))
                ->searchable()
                ->hideFromIndex(),

            Text::make('Route','route_no')
                ->onlyOnIndex()
                ->sortable(),

            Text::make('Station Name','name')
                ->rules(
                    [
                        'required','string'
                    ]
                )
                ->required()
                ->sortable(),
            Number::make('Comission %','commission_percent')
                  ->creationRules('required', 'numeric', 'min:0','max:100')
                  ->min(1)->max(100)
                  ->sortable()->hideFromIndex(),

            Text::make('Route No','route_no')
                ->hideFromIndex(),
            Text::make('Phone')
                ->hideFromIndex(),

            // Address Information:
            Text::make('Address','address_1')
                ->rules(['required','string'])
                ->sortable(),

            Text::make('City')
                ->required()
                ->rules([
                    'required','string'
                ])->sortable(),

            Text::make('State')
                ->rules([
                    'required','string'
                ])
                ->onlyOnForms(),
            Text::make('Postal Code','zip')
                ->required()
                ->rules([
                    'required','string'
                ])
                ->onlyOnForms(),

            File::make('Contract')->disk('public')
                ->rules([
                  'mimes:pdf','max:5000'
                ])
                ->acceptedTypes('.pdf'),
            Text::make('Comission (Previous Quarter)','pre_quarter_com',function (){
                if($this->sales->isEmpty()){
                    return null;
                }
                $now = now()->subQuarter(1);
                $sales = $this->sales->whereBetween('item_date', [$now->startOfQuarter()->toDateString(), $now->endOfQuarter()->addDay()->toDateString()])
                                     ->where('item_date','<>',$now->endOfQuarter()->addDay()->toDateString());

                if($sales->isEmpty()){
                    return null;
                }
                $cash = $sales->sum('cash');
                $credit = $sales->sum('credit');
                return round(((floatval($cash) + floatval($credit)) *  floatval($this->commission_percent)) / 100,2);
            })
            ->sortable('pre_quarter_com')->onlyOnIndex(),

            Text::make('Comission (This Quarter)','this_quarter_com',function (){
                if($this->sales->isEmpty()){
                    return null;
                }
                $now = now();
                $sales = $this->sales->whereBetween('item_date', [$now->startOfQuarter()->toDateString(), $now->endOfQuarter()->addDay()->toDateString()])
                                     ->where('item_date','<>',$now->endOfQuarter()->addDay()->toDateString());

                if($sales->isEmpty()){
                    return null;
                }
                $cash = $sales->sum('cash');
                $credit = $sales->sum('credit');
                return round(((floatval($cash) + floatval($credit)) *  floatval($this->commission_percent)) / 100,2);
            })
            ->sortable('this_quarter_com')->onlyOnIndex()
        ];

        $canMachines = auth()->user()->role_id == User::ROLE_OWNER || auth()->user()->can('machines');
        if($canMachines){
            array_push($fields, HasMany::make('Machines','machines','App\Nova\Machine'));
        }

        if ($canEditTime) {

            array_splice($fields, 5, 0, [Text::make('Time','OpeningHours')->exceptOnForms()->hideFromIndex()]);

            array_splice($fields, 7, 0, [
                TimeField::make('Opening Time', 'open_time')
                    ->rules([
                        'required',
                        'date_format:H:i'
                    ])
                    ->resolveUsing(function ($value, $resource, $attribute) {
                        return $value ? Carbon::parse($value)->format('H:i') : '06:00';
                    })
                    ->onlyOnForms()
            ]);
        
            array_splice($fields, 8, 0, [
                TimeField::make('Closing Time', 'close_time')
                    ->rules([
                        'required',
                        'date_format:H:i',
                        'after:open_time',
                    ])
                    ->resolveUsing(function ($value, $resource, $attribute) {
                        return $value ? Carbon::parse($value)->format('H:i') : '22:00';
                    })
                    ->readonly(function () use ($canEditTime) {
                        return !$canEditTime; // Make it readonly if $canEditTime is false
                    })
                    ->onlyOnForms()
            ]);
        }

        return $fields;
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function addressFields()
    {
        return [

            Textarea::make('Address','address_1')
                ->creationRules(['required','string']),
            Text::make('City')
                ->required()
                ->creationRules([
                    'required','string'
                ])
                ->onlyOnForms(),

            Text::make('State')
                ->required()
                ->rules([
                    'required','string'
                ])
                ->onlyOnForms(),
            Text::make('Postal Code','zip')
                ->required()
                ->creationRules([
                    'required','string'
                ])
                ->onlyOnForms(),
            Text::make('State'),
            Text::make('Postal Code')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
          (new DashboardAction())->onlyOnDetail(),
          new CommissionReport(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
          new ViewClientDashboard()
        ];
    }

}
