<?php

namespace App\Console\Commands\Logs;

use Illuminate\Console\Command;
use App\Models\Machine;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class MachinesStatusCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:machine-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $machines = Machine::get();

        $machineNumbers = [];    
        foreach($machines as $machine){
            

            if (Str::of($machine->credit)->isNotEmpty()) {
                array_push($machineNumbers,$machine->credit);
            } 

            if (Str::of($machine->cash_and_credit)->isNotEmpty()) {
                array_push($machineNumbers,$machine->cash_and_credit);
            } 
        }
        
        $results = DB::table('transactions')
        ->select('device_number', DB::raw('MAX(transaction_date) as last_transaction_at'))
        ->whereIn('device_number', $machineNumbers)
        ->groupBy('device_number')
        ->get();

        // Transform results for bulk insertion
        $logs = $results->map(function ($result) {
            return [
                'device_number' => $result->device_number, // Adjust this to match your machine_logs table
                'last_transaction_at' => $result->last_transaction_at,
            ];
        })->toArray();



        // Truncate the machine_logs table
        DB::table('machine_logs')->truncate();

        // Perform the bulk insert
        if (!empty($logs)) { // Check if logs are not empty
            DB::table('machine_logs')->insert($logs);
        } else {
            // Optional: Log or handle the case when there are no logs to insert
            \Log::info('No logs to insert into machine_logs.');
            return Command::FAILURE;
        }


        $creditQuery = DB::table('machines as m')
            ->select('m.id as machine_id','station_id as location_id','m.credit as machine_number', DB::raw("'credit' AS type"),'ml.device_number', 'ml.last_transaction_at')
            ->leftJoin('machine_logs as ml', 'm.credit', '=', 'ml.device_number')
            ->whereNotNull('m.credit')
            ->where('m.credit', '<>', '');

        $cashQuery = DB::table('machines as m')
            ->select('m.id as machine_id','station_id as location_id','m.cash_and_credit as machine_number', DB::raw("'cash_and_credit' AS type") ,'ml.device_number', 'ml.last_transaction_at')
            ->leftJoin('machine_logs as ml', 'm.cash_and_credit', '=', 'ml.device_number')
            ->whereNotNull('m.cash_and_credit')
            ->where('m.cash_and_credit', '<>', '');

        // Combine the two queries using union
        $results = $creditQuery->union($cashQuery)
            ->orderBy('machine_id')
            ->get();


        // Transform results for bulk insertion
        $logs = $results->map(function ($result) {
            return [
                'machine_id' => $result->machine_id,
                'machine_number' => $result->machine_number,
                'location_id' => $result->location_id,
                'type' => $result->type,
                'device_number' => $result->device_number, // Adjust this to match your machine_logs table
                'last_transaction_at' => $result->last_transaction_at,
                'created_at' => now()->toDateTimeString(),
            ];
        })->toArray();    

        

        // Truncate the machine_logs table
        DB::table('machine_logs')->truncate();

        // Perform the bulk insert
        if (!empty($logs)) { // Check if logs are not empty
            DB::table('machine_logs')->insert($logs);

        } else {
            // Optional: Log or handle the case when there are no logs to insert
            \Log::info('No logs to insert into machine_logs.');

        }

        $notLinkedResults = DB::table('transactions')
        ->select(
            DB::raw('null as machine_id'),
            DB::raw('null as machine_number'),
            DB::raw('null as location_id'),
            DB::raw('null as type'),
            'device_number',
            DB::raw('MAX(transaction_date) as last_transaction_at'),
            DB::raw('NOW() as created_at')  // Add current timestamp as created_at
        )
        ->whereNotIn('device_number', $machineNumbers)
        ->groupBy('device_number')
        ->get();

        // Check if the result is not empty before processing
        if ($notLinkedResults->isNotEmpty()) {
            // Convert the result to an array of associative arrays
            $notLinkedResultsArray = $notLinkedResults->map(function ($item) {
                return [
                    'machine_id' => $item->machine_id,
                    'machine_number' => $item->machine_number,
                    'location_id' => $item->location_id,
                    'type' => $item->type,
                    'device_number' => $item->device_number,
                    'last_transaction_at' => $item->last_transaction_at,
                    'created_at' => $item->created_at,
                ];
            })->toArray();

            // Insert into machine_logs if the array is not empty
            if (!empty($notLinkedResultsArray)) {
                DB::table('machine_logs')->insert($notLinkedResultsArray);
            }
        } 
        else {
            // Handle the case where no results were found (optional)
            \Log::info('No not linked results found.');
        }

        
        return Command::SUCCESS;
        
    }
}
