<?php

namespace App\Console\Commands\SheetDB;

use App\Http\Services\SheetDBService;
use App\Models\CoinCollection;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sheedb:sync-coins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $this->info('Syncing Coins.');

      $sheetSevice = new SheetDBService();

      $coinsData = $sheetSevice->getRows('Collections',env('SHEET_ROWS_LIMIT',500),'Last Updated DateTime','desc','date');

      foreach ($coinsData as $coin)
      {

        $data = [];
        array_push($data,[
          'location_id' => $coin->{'Location Address'},
          'collection_id' => $coin->{'Collections ID'},
          'technician_id' => $coin->{'Technician'},
          'coins_collected' => $coin->{'Total Coins collected'},
          'visit_date' => Carbon::parse($coin->DateTime),
        ]);
        CoinCollection::upsert($data,['collection_id'],['coins_collected','visit_date','technician_id']);
      }
      $this->info('Coins Synced.');
    }
}
