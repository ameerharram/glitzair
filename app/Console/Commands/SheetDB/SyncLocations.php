<?php

namespace App\Console\Commands\SheetDB;

use App\Http\Services\SheetDBService;
use App\Models\Station;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sheetdb:sync-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

       $stations = Station::all();
       $data = [];
       foreach ($stations as $station)
       {
         array_push($data,[
           'Location ID' => $station->id,
           'Name Of The Location' => $station->name,
           'Location Address' => $station->address_1.', '.$station->city.', '. $station->state.' '.$station->zip
         ]);
       }

       $sheetService = new SheetDBService();
       $sheetService->createRows($data);
    }
}
