<?php

namespace App\Console\Commands\SheetDB;

use App\Http\Services\SheetDBService;
use App\Models\CoinCollection;
use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncLocationRoute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sheedb:sync-location-route';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $this->info('Syncing Routes.');

      $sheetSevice = new SheetDBService();

      $locationData = $sheetSevice->getRows('Locations',env('SHEET_ROWS_LOCATION_LIMIT',2000),'Last Updated DateTime','desc','date');

      foreach ($locationData as $location) {

        Station::where('id', $this->parseStringToInt($location->{'Location ID'}))
                ->update(['route_no' => trim($location->{'Route'})]);
      }

      $this->info('Routes Synced.');
    }

    private function parseStringToInt($string) {
      if (is_numeric($string) && intval($string) == $string) {
        return intval($string);
      }
      return null;
    }
}
