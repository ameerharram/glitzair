<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MonthlyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:send-monthly-stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will send report via email every mid and end of month.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $now = now()->toDateString();

      $data = [
        'url' => route('download.monthly.report',['current_date' => $now ]),
        'date' => now()->format('jS F, Y')
      ];

      Mail::send(new \App\Mail\MonthlyReport($data));

      $this->info('Monthly Email Report Sent successfully: ' . $now);

      return Command::SUCCESS;
    }
}
