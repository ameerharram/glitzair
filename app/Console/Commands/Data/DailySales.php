<?php

namespace App\Console\Commands\Data;

use App\Services\MachineService;
use Illuminate\Console\Command;

class DailySales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:daily-sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '2048M');
        $machine = new MachineService();
        $machine->recordMachinesDaySales();
        return 0;
    }
}
