<?php

namespace App\Console\Commands\Data;

use App\Models\QuarterLog;
use App\Models\Station;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class QuarterlyLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:quarterly-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command logs quarterly sales.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      // Step 1: Get the Current Date
      $sub = env('LAST_SUB_QUARTER_NUMBER',1);
      $lastQuarter =now()->subQuarter($sub);
      $quarterNo = now()->subQuarter($sub)->quarter;
      $year = now()->subQuarter($sub)->year;

      $lastQuarterDates = [$lastQuarter->startOfQuarter()->toDateString(),$lastQuarter->endOfQuarter()->toDateString(),$lastQuarter->endOfQuarter()->addDay()->toDateString()];
      $stations =  Station::all();

      foreach ($stations as $station)
      {
        $quarter = QuarterLog::where('location_id',$station->id)
                              ->where('quarter_start_date',$lastQuarterDates[0])
                              ->where('quarter_end_date', $lastQuarterDates[1])
                              ->first();

        if(!$quarter){
          $quarter = new QuarterLog();
        }

        $quarter->location_id = $station->id;
        $quarter->year = $year;
        $quarter->quarter_no =  $quarterNo;
        $quarter->quarter_start_date = $lastQuarterDates[0];
        $quarter->quarter_end_date = $lastQuarterDates[1];
        $quarter->commission = $station->commission_percent;
        $quarter->amount = $this->commissionCalculator($station,$lastQuarterDates[0],$lastQuarterDates[2]);
        $quarter->save();



      }

      return Command::SUCCESS;
    }

    private function commissionCalculator($station,$startDate,$endDate)
    {
      $sales = $station->sales->whereBetween('item_date', [$startDate, $endDate])
        ->where('item_date','<>',$endDate);
      $commission  = 0;
      if(!$sales->isEmpty()){
        $cash = $sales->sum('cash');
        $credit = $sales->sum('credit');

        $commission =  round(((floatval($cash) + floatval($credit)) *  floatval($station->commission_percent)) / 100,2);
      }
      return $commission;
    }
}
