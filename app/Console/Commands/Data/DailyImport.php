<?php

namespace App\Console\Commands\Data;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DailyImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:daily-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will execute multiple command to daily import data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '2048M');

        $this->info('Importing Seedlive Started.');
        Artisan::call('seedlive:import');
        $this->info('Importing Nayax Started.');
        Artisan::call('nayax:import');
        $this->info('Daily Sales started.');
        Artisan::call('data:daily-sales');
        $this->info('Process ended.');
    }
}
