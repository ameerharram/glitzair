<?php

namespace App\Console\Commands\Data\Nayax;

use App\Services\MachineService;
use Illuminate\Console\Command;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nayax:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will import data from nayax.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '2048M');
        $machine = new MachineService();
        $machine->importNayaxXML();
        return 0;
    }
}
