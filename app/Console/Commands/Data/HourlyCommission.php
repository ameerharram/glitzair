<?php

namespace App\Console\Commands\Data;

use App\Models\Station;
use App\Services\MachineService;
use Illuminate\Console\Command;

class HourlyCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:hourly-commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      Station::chunk(50, function ($stations) {
        foreach ($stations as $station) {
          $station->pre_quarter_com = $station->previous_quarter_comm;
          $station->this_quarter_com = $station->this_quarter_comm;
          $station->save();
        }
      });
    }
}
