<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

      if(env('SCRIPT_TIMMER_TEST') == 'on')
      {
        $schedule->command('data:daily-import')->everyMinute();
      }
      else
      {
        $schedule->command('data:daily-import')->dailyAt('06:00');
      }

      $schedule->command('log:machine-status')->dailyAt('08:00');
      

      $schedule->command('data:hourly-commission')->everyTwoHours();

      $schedule->command('sheedb:sync-location-route')->dailyAt('06:30');

      $schedule->command('sheedb:sync-coins')->dailyAt('06:00');
      $schedule->command('sheedb:sync-coins')->dailyAt('18:00');

      $schedule->command('data:quarterly-log')->quarterlyOn(2, '09:00');

      //Monthly Report
      $schedule->command('report:send-monthly-stats')->monthly(15)->at('07:00');
      $schedule->command('report:send-monthly-stats')->lastDayOfMonth()->at('07:00');


      
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
