<?php

namespace App\Providers;

use Ameer\SalesByPeriod\SalesByPeriod;
use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Ameer\UserPermissionManager\UserPermissionManager; 
use App\Nova\Coins;
use App\Nova\Collection;
use App\Nova\Dashboards\Main;
use App\Nova\Location;
use App\Nova\Logs;
use App\Nova\Machine;
use App\Nova\Sale;
use App\Nova\User as NovaUser;
use Illuminate\Support\Facades\Route;
use Laravel\Nova\Nova;



class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        Nova::serving(function (){
          User::observe(UserObserver::class);
        });
        // Override the Nova dashboard URL
        Nova::path('/dashboards/v1');  // Set the desired path for the Nova dashboard        
    }


    /**
     * Register the application's Nova resources.
     */
    protected function resources(): void
    {
        // Only register the Coins resource if the user has the 'view coins' permission
        $role = auth()->user()->role_id;

        if($role == User::ROLE_OWNER){
            Nova::resourcesIn(app_path('Nova'));
        }
        elseif($role == User::ROLE_ADMIN)
        {

            // Register other resources unconditionally
            if (auth()->user()->can('coins')) {
                Nova::resources([
                    Coins::class,
                ]);
            }
            
            if (auth()->user()->can('collections')) {
                Nova::resources([
                    Collection::class,
                ]);
            }

            if (auth()->user()->can('logs')) {
                Nova::resources([
                    Logs::class,
                ]);
            }

            if (auth()->user()->can('machines')) {
                Nova::resources([
                    Machine::class,
                ]);
            }

            if (auth()->user()->can('sale')) {
                Nova::resources([
                    Sale::class,
                ]);

                Nova::resources([
                    Location::class,
                ]);
                
            }elseif(auth()->user()->can('locations')){
                Nova::resources([
                    Location::class,
                ]);
            }
            
            if (auth()->user()->can('users')) {
                Nova::resources([
                    NovaUser::class,
                ]);
            }
            
        }    
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        // Override the root route to redirect to your custom dashboard or page
         Route::get('/', function () {
            return redirect('/dashboards/v1'); // Replace this with your target URL
        });

        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
        
        // Define a custom route to redirect the default Nova dashboard
        Route::get('/dashboards/main', function () {
            // Redirect to your custom page
            return redirect('/dashboards/v1'); // Replace with your custom URL
        }); 
        
        
    }

    /**
     * Configure the Nova authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();

        Nova::auth(function ($request) {

            return Gate::check('viewNova', [$request->user()]);
        });
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            if($user && $user->hasAnyRole('owner','admin')){
                return true;
            }
            return  false;
            //return in_array($user->email, []);
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        $dashboards = [];
        if (auth()->user()->can('dashboard') || auth()->user()->role_id == User::ROLE_OWNER) {
            array_push($dashboards,new Main());
        }
        return $dashboards;
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
         // Only Onwer can view tool
         $role = auth()->user()->role_id;
         if($role == User::ROLE_OWNER){
            return [
                new UserPermissionManager,     
            ];
         }
        return [];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
