<?php

namespace App\Traits;

use App\Models\Station;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait ClientTrait
{
    private function clientCheck($clientUuid)
    {
        $user = Auth::user();

        if( $user->client_uuid != $clientUuid && $user->hasAnyRole('owner','admin') ) {
            $clientId = optional(User::where('uuid',$clientUuid)->first())->id;
        }elseif( $user->client_uuid == $clientUuid )
        {
            $clientId = optional(User::where('uuid',$user->client_uuid)->first())->id;
        }else{
            abort(401);
        }
        return $clientId;
    }

    public function clientCheckStations($clientUuid)
    {
      $user = Auth::user();
      $stations = Station::active();
      if( $user->client_uuid != $clientUuid && $user->hasAnyRole('owner','admin') ) {
        $clientId = optional(User::where('uuid',$clientUuid)->first())->id;
        $stations = $stations->where('owner_id',$clientId);
      }elseif( $user->uuid == $clientUuid )
      {
        $clientId = optional(User::where('uuid',$user->client_uuid)->first())->id;
        $stations = $stations->where('owner_id',$clientId);
      }
      elseif( $user->uuid != $clientUuid && $user->client_uuid == $clientUuid && $user->hasAnyRole('sub-client') )
      {
        //$clientId = optional(User::where('uuid',$user->client_uuid)->first())->id;
        $stations = $stations->whereIn('id',$user->mlocs()->pluck('station_id'));
      }
      else{
        abort(401);
      }
      return $stations;
    }

}
