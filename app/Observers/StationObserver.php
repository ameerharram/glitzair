<?php

namespace App\Observers;

use App\Http\Services\SheetDBService;
use App\Models\Station;
use SheetDB\SheetDB;

class StationObserver
{
    /**
     * Handle the Station "created" event.
     *
     * @param  \App\Models\Station  $station
     * @return void
     */
    public function created(Station $station)
    {
      $sheetService = new SheetDBService();
      $sheetService->createRows([
        'Location ID' => $station->id,
        'Name Of The Location' => $station->name,
        'Location Address' => $station->address_1.', '.$station->city.', '. $station->state.' '.$station->zip,
        'Route' => $station->route_no
      ]);
    }

    /**
     * Handle the Station "updated" event.
     *
     * @param  \App\Models\Station  $station
     * @return void
     */
    public function updated(Station $station)
    {
        if($station->isDirty('name','address_1','city','state','zip','route_no'))
        {
          $sheetService = new SheetDBService();
          $code = $sheetService->updateRows([
            'Name Of The Location' => $station->name,
            'Location Address' => $station->address_1.', '.$station->city.', '. $station->state.' '.$station->zip,
            'Route' => $station->route_no
          ],$station->id);

          if($code == 404){
            $sheetService->createRows([
              'Location ID' => $station->id,
              'Name Of The Location' => $station->name,
              'Location Address' => $station->address_1.', '.$station->city.', '. $station->state.' '.$station->zip,
              'Route' => $station->route_no
            ]);
          }
        }
    }

    /**
     * Handle the Station "deleted" event.
     *
     * @param  \App\Models\Station  $station
     * @return void
     */
    public function deleted(Station $station)
    {
      $sheetService = new SheetDBService();
      $sheetService->deleteRow($station->id);
    }

    /**
     * Handle the Station "restored" event.
     *
     * @param  \App\Models\Station  $station
     * @return void
     */
    public function restored(Station $station)
    {
        //
    }

    /**
     * Handle the Station "force deleted" event.
     *
     * @param  \App\Models\Station  $station
     * @return void
     */
    public function forceDeleted(Station $station)
    {

    }
}
