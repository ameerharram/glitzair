<?php

namespace App\Http\Controllers;

use App\Http\Services\SheetDBService;
use App\Models\Machine;
use App\Models\TeamMember;
use http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Nova\Nova;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;
use SheetDB\SheetDB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('signupViaInvite','sheetDb');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($uuid)
    {
        return view('home');
    }

    public function location($uuid,$locationId)
    {
        return view('location');
    }

    public function myTeams()
    {
      return view('teams');
    }

    public function profile()
    {
      return view('profile');
    }

    public function check403()
    {
        $user = auth()->user();
        $redirectPath =  Nova::path();

        if( !$user->hasAnyRole('owner','admin') )
        {
            $stations = $user->stations()->get()->toArray();
            if( count($stations) > 1 ){
                $redirectPath = route('home',['uuid' => $user->client_uuid]);
            }
            else
            {
                $redirectPath = route('location',['uuid' => $user->client_uuid, 'id' => $stations[0]['id']]);
            }
        }

        redirect()->setIntendedUrl($redirectPath);

        return redirect()->intended($redirectPath);
    }

    public function signupViaInvite($inviteCode)
    {
      if(auth()->check()){
        return redirect('/');
      }

      $member = TeamMember::where('invite_code',$inviteCode)
                          ->where('accpeted_at',null)
                          ->firstOrfail();

      return view('invite_signup',['member' => $member]);
    }

    public function sheetDb(){
      $sheet = new SheetDBService();
      $status = $sheet->createRows([
        [
        'Location ID' => 'SheedDB_'.Str::random(2),
        'Name Of The Location' => Str::random(),
        'Location Address' => Str::random(),
      ],
        [
          'Location ID' => 'SheedDB_'.Str::random(2),
          'Name Of The Location' => Str::random(),
          'Location Address' => Str::random(),
        ]
      ]);
    }



  public function downloadMonthlyReport($currentDate)
  {

    $validator = Validator::make(['date_input'=> $currentDate ], [
      'date_input' => 'required|date|date_format:Y-m-d',
    ]);

    if ($validator->fails()) {
      dd('something wrong!');
    }

    // Retrieve $locations data
    $locations = DB::table('machines as m')
      ->select((DB::raw("m.station_id  as location_id, if(s.name is null,if(m.station_id is null,'No Location Linked','Location Deleted'),s.name) as location_name , m.id as machine_id,if(m.machine_type = 2, 'NAYAX','SEEDLIVE') as machine_type, m.name  as machine_name,m.credit,m.cash_and_credit, IF (s.id is null, 'Not Attached','Attached') as attached_to_location")))
      ->leftJoin('stations  as s', 's.id', '=', 'm.station_id')
      ->get();

    config(['database.connections.mysql.strict' => false]);

    DB::reconnect();
    // Retrieve $machines data
    $machines = DB::table('transactions as t')
      ->select((DB::raw("t.device_number, if(t.machine_type = 2, 'NAYAX','SEEDLIVE') as machine_type, if(m.id is null, 'Machine not Added',  if(m.station_id is null, 'Location not Attached' , if(s.id is null, 'Location Deleted', 'Unknown')   ) ) as reason,MAX(t.transaction_date) as last_date_recorded ")))
      ->leftJoin('machines as m', function ($join) {
        $join->on('m.credit', '=', DB::raw('t.device_number'))
          ->orWhere('m.cash_and_credit', '=', DB::raw('t.device_number'));
      })
      ->leftJoin('stations as s', 's.id', '=', 'm.station_id')
      //->where('t.status', 1)
      ->whereRaw("date(t.transaction_date)  >= ('{$currentDate}' - INTERVAL 30 DAY)")
      ->whereRaw("if(m.id is null, true,(t.device_number = m.cash_and_credit or (t.device_number = m.credit and t.transaction_type = 1)))")
      ->groupBy('t.device_number')
      ->get();

    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();

    $sheets = new SheetCollection([
      'Location data with machines'=> $locations,
      'Machines we are reciving data'=> $machines
    ]);

    $name = 'Report '.$currentDate.'.xls';

    return  (new FastExcel($sheets))->download($name);

  }



}
