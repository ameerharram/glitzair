<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\MachineLog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class MachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadMachineReport(){
   
        $logs = MachineLog::select(
                            'machine_logs.id',
                            'l.name as location_name',
                              DB::raw("
                                      CASE
                                          WHEN last_transaction_at IS NULL THEN 0
                                          WHEN machine_number IS NULL THEN 2
                                          WHEN TIMESTAMPDIFF(HOUR, last_transaction_at, NOW()) >= 48 THEN 1
                                          ELSE 3
                                       END AS status,
                                       COALESCE(machine_logs.device_number, machine_logs.machine_number) as machine_number
                                    "),
                              'last_transaction_at'
                            )
                            ->leftJoin('stations as l', 'machine_logs.location_id', '=', 'l.id')
                            ->where(function ($query) {
                              $query->whereNull('last_transaction_at')
                                    ->orWhere(DB::raw('TIMESTAMPDIFF(HOUR, last_transaction_at, NOW())'), '>=', 48)
                                    ->orwhereNull('machine_number');
                          })->orderBy('last_transaction_at')->get();  

        $name = 'Machine Report '.Carbon::now()->format('YmdHi'). '.csv';
        return (new FastExcel($logs))->download($name, function ($log)  {
          return [
            'ID' => $log->id,
            'Machine_Number' =>  "\u{200B}". $log->machine_number,
            'Location_Name' => $log->location_name,
            'Status' => $log->status == 0 ? 'Never Received' : ($log->status == 1 ? 'Inactive - 48+ Hours' : ($log->status == 2 ? 'Not Linked':  'Active - Recent')),
            'Last_Transaction_At' => $log->last_transaction_at,
          ];
        });                  
                  
    }
}
