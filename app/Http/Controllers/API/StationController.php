<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\SupportEmail;
use App\Models\Machine;
use App\Models\QuarterLog;
use App\Models\Sale;
use App\Models\Station;
use App\Models\User;
use App\Traits\ClientTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Location;
use Rap2hpoutre\FastExcel\FastExcel;

class StationController extends Controller
{
    use ClientTrait;

    public function __construct()
    {
        $this->middleware('auth')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($clientUuid)
    {
        $stations = $this->clientCheckStations($clientUuid);

        $stations = $stations->get();

        return $stations;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($clientUUID, $locationId)
    {
        $stations = $this->clientCheckStations($clientUUID);

        $station = $stations->where('id',$locationId)
                            ->first();
        return $station;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $clientUuid,$locationId)
    {
        $request->validate([
          'name' => 'required|string',
          'phone' => 'required|string',
          'address_1' => 'required|string',
          'address_2' => 'nullable|string',
          'city' => 'required|string',
          'state' => 'required|string',
          'postal_code' => 'required|string',
        ]);

        $station = $this->clientCheckStations($clientUuid);

        $station = $station->where('id',$locationId)
                          ->firstOrFail();

        $station->name = $request->name;
        $station->phone = $request->phone;
        $station->address_1 = $request->address_1;
        $station->address_2 = $request->address_2;
        $station->city = $request->city;
        $station->state = $request->state;
        $station->zip = $request->postal_code;

        $station->save();

        return $station;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function locationStats($clientUUID, $locationId)
    {
        $clientId = $this->clientCheck($clientUUID);

        $station = Station::where('owner_id',$clientId)
            ->where('id',$locationId)
            ->first();

        $daily = $this->getTotalEarnings($station->id, 'daily');
        $monthly = $this->getTotalEarnings($station->id, 'monthly');
        //$lifetime = $this->getTotalEarnings($station->id, '');

        return  [
            'daily' => round($daily,2),
            'monthly' => round($monthly,2),
            'quarter' => round($station->this_quarter,2),
        ];
    }

    /**
     * @param $locationId
     * @param $interval
     * @return mixed
     */
    private function getTotalEarnings($locationId, $interval)
    {
        $data = Sale::where('station_id', $locationId);

        if ($interval == 'daily') {
            $data->where('item_date', '>=', Carbon::yesterday());
        } else if ($interval == 'monthly') {

            $currentMonthFrom = now()->startOfMonth()->toDateString();

            $currentMonthTo = now()->endOfMonth()->toDateString();

            $data->whereBetween('item_date', [$currentMonthFrom, $currentMonthTo]);
        }

        $filteredData = $data->select(DB::raw("SUM(cash) as cash"), DB::raw("SUM(credit)as credit"))->first();

        return $filteredData->cash + $filteredData->credit;
    }

    public  function locationSalesDetail(Request $request,$clientUUID, $locationId)
    {
        $clientId = $this->clientCheck($clientUUID);
        $station = Station::with('activeMachines')->where('owner_id',$clientId)
                        ->where('id',$locationId)
                        ->first();

        $currentMonthFrom = now()->startOfMonth()->toDateString();
        $currentMonthTo = now()->endOfMonth()->toDateString();

        if($request->from && $request->to) {
            $currentMonthFrom = $request->from;
            $currentMonthTo = $request->to;
        }

        $data = $this->getDataNew($locationId, $currentMonthFrom, $currentMonthTo);


      $groupedCollection = collect($data)->groupBy('machine_id');

      $salesData = [];
      foreach ($groupedCollection->toArray() as $key=>$items){
        $totalCredit = collect($items)->sum('credit');
        $totalCash = collect($items)->sum('cash');
        //dd($items);
        array_push($salesData,[
          "cash" => $totalCash,
          "credit" => $totalCredit,
          "machine_name" => optional($items[0])->machine_name
        ]);
      }

      $totalCredit = collect($salesData)->sum('credit');
      $totalCash = collect($salesData)->sum('cash');

        return [
            'data' => $salesData,
            'totalCredit' => $totalCredit,
            'totalCash' => $totalCash,
        ];
    }

  /**
   * @param $locationId
   * @param $fromDate
   * @param $toDate
   * @return array
   */
    protected function getDataNew($locationId, $fromDate, $toDate)
    {

      $saleData = Sale::whereBetween('item_date', [$fromDate, $toDate])->where('station_id', $locationId)->groupBy('machine_number')
                            ->select(['machine_number', DB::raw("SUM(cash) as total_cash"), DB::raw("SUM(credit) as total_credit")])->get();

      $data = [];


      foreach ($saleData as $sale)
      {
        $obj = new \stdClass();
        $obj->cash = round($sale->total_cash,2);
        $obj->credit = round($sale->total_credit,2);
        $obj->machine_number = $sale->machine_number;
        $machineNumber = $sale->machine_number;
        $machine = Machine::where('station_id', $locationId)->where('status', 1)
                          ->where(function($query ) use ($machineNumber) {
                            $query->where('credit', $machineNumber)
                              ->orWhere('cash_and_credit',$machineNumber);
                          })
                          ->first();

        $obj->machine_name = '......';
        $obj->machine_id = 0;

        if($machine)
        {
          $obj->machine_name = $machine->name;
          $obj->machine_id = $machine->id;
        }

        array_push($data, $obj);
      }

    return $data;
  }

    /**
     * @param $locationId
     * @param $fromDate
     * @param $toDate
     * @return array
     */
    protected function getData($locationId, $fromDate, $toDate)
    {
        $machines = Machine::where('station_id', $locationId)->where('status', 1)->get(['credit', 'cash_and_credit','name']);
        $data = [];

        foreach ($machines as $machine) {
            $cashDevice = $machine->credit;
            $creditDevice = $machine->cash_and_credit;

            $saleData = Sale::whereIn('machine_number', [$cashDevice, $creditDevice])->whereBetween('item_date', [$fromDate, $toDate])->where('station_id', $locationId)->groupBy('machine_number')->select(['machine_number', DB::raw("SUM(cash) as total_cash"), DB::raw("SUM(credit) as total_credit")])->get();

            $obj = new \stdClass();
            $obj->cash = round($saleData->sum('total_cash'),2);
            $obj->credit = round($saleData->sum('total_credit'),2);
            $obj->machine_name = $machine->name;
            if($machine->credit){
              $obj->machine_number = $machine->credit;
            }

            if($machine->credit && $machine->cash_and_credit)
            {
              $obj->machine_number = ' / '.$machine->cash_and_credit ;
            }else
            {
              $obj->machine_number = $machine->cash_and_credit;
            }

			      array_push($data, $obj);
        }

        return $data;
    }

    // get Location names
    protected function getLocationName($deviceId)
    {
        return Machine::where('credit', $deviceId)
            ->where('status', 1)
            ->orWhere('cash_and_credit', $deviceId)
            ->where('status', 1)->value('name');
    }


  public function getSalesReportFile(Request $request, $clientUuid,$locationId)
  {
    $station = $this->clientCheckStations($clientUuid);

    $currentMonthFrom = now()->startOfMonth()->toDateString();
    $currentMonthTo = now()->endOfMonth()->toDateString();

    if($request->from && $request->to) {
      $currentMonthFrom = $request->from;
      $currentMonthTo = $request->to;
    }

    $station = Station::where('id',$locationId)
                        ->firstOrFail();
    config(['database.connections.mysql.strict' => false]);
    DB::reconnect();

    $data = DB::table('sales as s')
              ->leftJoin('machines as m', function ($join) {
                $join->on('s.station_id', '=', 'm.station_id')
                      ->where('m.status', 1)
                      ->on(function ($join){
                        $join->on('s.machine_number',  '=', 'm.credit')
                          ->orOn('s.machine_number',  '=', 'm.cash_and_credit');
                      });
              })
              ->select(
                's.item_date',
                DB::raw("IFNULL(m.name,'Other') as machine_name, sum(s.cash) as cash , sum(s.credit) as credit"),
              )
              ->where('s.station_id',$station->id)
              ->whereBetween('s.item_date', [$currentMonthFrom, $currentMonthTo])
              ->where(function ($query){
                $query->where('s.cash' ,'>',0)
                      ->orWhere('s.credit','>',0);
              })
              ->groupByRaw('m.id, s.item_date')
              ->get();

    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();

    $name = ucfirst($station->name) .' '. $currentMonthFrom  .' to '. $currentMonthTo. '.xls';

    return (new FastExcel($data))->download($name, function ($item) {
      return [
        'Date' => $item->item_date,
        'Machine' => $item->machine_name,
        'Cash' => $item->cash,
        'Credit' => $item->credit,
        'Total' => $item->cash + $item->credit
      ];
    });
  }

  public function sendSupport(Request $request,$clientUuid,$locationId)
  {
    $request->validate([
      'subject' => 'required',
      'message' => 'required'
    ]);

    $location = Station::where('id',$locationId)->firstOrFail();
    $data = [
      'user_id' => \auth()->user()->id,
      'user_name' =>  \auth()->user()->name,
      'user_email' =>  \auth()->user()->email,
      'location_id' => $location->id,
      'location_name' => $location->name,
      'location_ownerid' => $location->owner_id,
      'location_phone' => $location->phone
    ];
    Mail::to(env('SUPPORT_EMAIL'))->send(new SupportEmail($request,$data));
  }

  public function graphData(Request $request,$clientUuid,$locationId)
  {
    $request->validate([
      'type'=> 'required|in:month,quarter,year'
    ]);

    $currentRange = new \stdClass();
    $previousRange = new \stdClass();
      switch ($request->type)
      {
        case 'month':

          $currentRange->from = now()->startOfMonth();
          $currentRange->to = now()->endOfMonth();

          $previousRange->from = now()->previous('month')->startOfMonth();
          $previousRange->to = now()->previous('month')->endOfMonth();
          break;
        case 'quarter':
          $currentRange->from = now()->startOfQuarter();
          $currentRange->to = now()->endOfQuarter();

          $previousRange->from = now()->subQuarter(1)->startOfQuarter();
          $previousRange->to = now()->subQuarter(1)->endOfQuarter();
          break;
        case 'year':

          $currentRange->from = now()->startOfYear();
          $currentRange->to = now()->endOfYear();

          $previousRange->from = now()->previous('year')->startOfYear();
          $previousRange->to = now()->previous('year')->endOfYear();
          break;

      }

//      return [
//        'current' => [
//          $currentRange->from->toDateTimeString(),
//          $currentRange->to->toDateTimeString(),
//        ],
//        'previous' => [
//          $previousRange->from->toDateTimeString(),
//          $previousRange->to->toDateTimeString(),
//        ],
//
//      ];

      $current = $this->getFormatedData($locationId, $request->type,$currentRange);
      $previous = $this->getFormatedData($locationId, $request->type,$previousRange);
      return compact('current','previous');
  }

  private function getFormatedData($locationId, $type, $range)
  {
    config(['database.connections.mysql.strict' => false]);
    DB::reconnect();

    $query = Sale::where('station_id',$locationId)
                 ->whereBetween('item_date',[$range->from,$range->to]);

    switch ($type)
    {
      case 'month':
        $query = $query->select(DB::raw('day(item_date) as x'),DB::raw('sum( cash + credit ) as y'))
                        ->groupBy(DB::raw('day(item_date)')); ;
        break;
      case 'quarter':
        $query = $query->select(DB::raw('month(item_date) as x'),DB::raw('sum( cash + credit ) as y'))
                       ->groupBy(DB::raw('month(item_date)'));
        break;
      case 'year':
        $query = $query->select(DB::raw('month(item_date) as x'),DB::raw('sum( cash + credit ) as y'))
                      ->groupBy(DB::raw('month(item_date)'));
    }
    $data = $query->orderBy('item_date')->get();
    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();
    return $data;
  }

  public function getAdminGrapSalesData(Request $request)
  {
    $request->validate([
      'type'=> 'required|in:last_7_days,last_30_days,last_12_months'
    ]);

    $currentRange = new \stdClass();
    $type = $request->type;

    switch ($type)
    {
      case 'last_7_days':

        $currentRange->from = now()->subDays(7);
        $currentRange->to = now()->subDays(1);
        break;

      case 'last_30_days':
        $currentRange->from = now()->subDays(30);
        $currentRange->to = now()->subDays(1);
        break;

      case 'last_12_months':
        $currentRange->from = now()->subMonths(12)->startOfMonth();
        $currentRange->to = now()->subMonths(1)->endOfMonth();
        break;
    }

    return  $this->getAdminFormatedData($type,$currentRange);

  }

  private function getAdminFormatedData($type, $range)
  {
    config(['database.connections.mysql.strict' => false]);
    DB::reconnect();

    $query = Sale::whereBetween('item_date',[$range->from->toDateString(),$range->to->toDateString()]);

    switch ($type)
    {
      case 'last_7_days':
        $query = $query->select(DB::raw("concat( day(item_date),' ', DATE_FORMAT(item_date,'%b') ) as label"),DB::raw('round(sum( cash ),2) as cash, round(sum( credit ),2) as credit'))
          ->groupBy(DB::raw('day(item_date)'));
        break;
      case 'last_30_days':
        $query = $query->select(DB::raw("concat( day(item_date),' ', DATE_FORMAT(item_date,'%b') ) as label"),DB::raw('round(sum( cash ),2) as cash, round(sum( credit ),2) as credit'))
          ->groupBy(DB::raw('day(item_date)'));
        break;
      case 'last_12_months':
        $query = $query->select(DB::raw("concat( DATE_FORMAT(item_date,'%b'),' ', year(item_date)) as label"),DB::raw('round(sum( cash ),2) as cash, round(sum( credit ),2) as credit'))
          ->groupBy(DB::raw('month(item_date)'));
    }
    $data = $query->orderBy('item_date')->get();

    $record = [
              'label' => $query->pluck('label'),
              'cash' => $query->pluck('cash'),
              'credit' => $query->pluck('credit'),
              ];

    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();
    return $record;
  }

  public function getAdminGrapSalesYearlyData(Request $request)
  {
    $currentRange = new \stdClass();
    $previousRange = new \stdClass();

    $type = 'year';

    switch ($type)
    {
      case 'year':
        $currentRange->from = now()->startOfYear();
        $currentRange->to = now()->endOfYear();

        $previousRange->from = now()->previous('year')->startOfYear();
        $previousRange->to = now()->previous('year')->endOfYear();
        break;
    }

    $current = $this->getAdminYearlyFormatedData($type,$currentRange)->pluck('y');
    $previous = $this->getAdminYearlyFormatedData($type, $previousRange)->pluck('y');

    return compact('current','previous');

  }


  private function getAdminYearlyFormatedData( $type, $range)
  {
    config(['database.connections.mysql.strict' => false]);
    DB::reconnect();

    $query = Sale::whereBetween('item_date',[$range->from->toDateString(),$range->to->toDateString()]);

    switch ($type)
    {
      case 'year':
        $query = $query->select(DB::raw('month(item_date) as x'),DB::raw('round(sum( cash + credit ),2) as y'))
                       ->groupBy(DB::raw('month(item_date)'));
        break;
    }
    $data = $query->orderBy('item_date')->get();
    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();
    return $data;
  }

  public function getLocationPerformance(Request $request)
  {
    $request->validate([
      'type'=> 'required|in:last_7_days,last_30_days,last_12_months',
      'order' => 'required|in:top_10,least_10'
    ]);

    $currentRange = new \stdClass();
    $type = $request->type;
    $order = $request->order;
//    $type = 'last_12_months';
//    $order = 'top_10';

    switch ($type)
    {
      case 'last_7_days':

        $currentRange->from = now()->subDays(7);
        $currentRange->to = now()->subDays(1);
        break;

      case 'last_30_days':
        $currentRange->from = now()->subDays(30);
        $currentRange->to = now()->subDays(1);
        break;

      case 'last_12_months':
        $currentRange->from = now()->subMonths(12)->startOfMonth();
        $currentRange->to = now()->subMonths(1);
        break;
    }
   
    return  $this->getAdminPerformanceData($type,$currentRange,$order);
  }

  private function getAdminPerformanceData($type, $range, $order , $limit = 10)
  {
    config(['database.connections.mysql.strict' => false]);
    DB::reconnect();

    $query = Sale::whereBetween('item_date',[$range->from->toDateString(),$range->to->toDateString()])
                  // ->whereNot(function ($query){
                  //   $query->where('cash','<>',0)
                  //         ->where('credit','<>',0);
                  // })
                  ->whereHas('station')
                 ->select('station_id',DB::raw('round(sum( cash + credit ),2) as total'))
                 ->groupBy('station_id');

    if($order == 'top_10') {
      $query = $query->orderBy(DB::raw('round(sum( cash + credit ),2)'),'desc');
    }

    if($order == 'least_10') {
      $query = $query->orderBy(DB::raw('round(sum( cash + credit ),2)'),'asc');
    }

    $data = $query->limit($limit)->get();
    $data->load('station');

    //$data = $data->shuffle();

    $record = [
      'lables' => $data->pluck('station.name'),
      'x' => $data->pluck('total'),
      'range' => [$range->from->toDateString(),$range->to->toDateString()]
    ];
    config(['database.connections.mysql.strict' => true]);
    DB::reconnect();
    return $record;
  }


  public function downloadCommissionReport(){

    // Step 1: Get the Current Date
    $lastQuarter =now()->subQuarter(1);
    $secondLastQuarter = now()->subQuarter(2);
    $thirdLastQuarter = now()->subQuarter(3);


    $dates = [
      'lastQuarterStartDate' => [$lastQuarter->startOfQuarter()->toDateString(), $lastQuarter->endOfQuarter()->toDateString()],
      'secondLastQuarterStartDate' => [$secondLastQuarter->startOfQuarter()->toDateString(), $secondLastQuarter->endOfQuarter()->toDateString()],
      'thirdLastQuarterStartDate' => [$thirdLastQuarter->startOfQuarter()->toDateString(), $thirdLastQuarter->endOfQuarter()->toDateString()],
    ];

    $stations =  Station::all();
      $name = 'Commission Report '.Carbon::now()->format('YmdHi'). '.csv';
      return (new FastExcel($stations))->download($name, function ($station) use ($dates) {
        return [
          'ID' => $station->id,
          'Name' => $station->name,
          'Route' => $station->route_no,
          'Address' => $station->address_1,
          'Commission %' => $station->commission_percent,
          'Last Quarter' => $this->commissionAmount($station,$dates['lastQuarterStartDate'][0],$dates['lastQuarterStartDate'][1]),
          'Second Last Quarter' => $this->commissionAmount($station,$dates['secondLastQuarterStartDate'][0],$dates['secondLastQuarterStartDate'][1]),
          'Third Last Quarter' => $this->commissionAmount($station,$dates['thirdLastQuarterStartDate'][0],$dates['thirdLastQuarterStartDate'][1]),
        ];
      });
  }

  private function commissionAmount($station,$startDate,$endDate)
  {
    $commission  = 0;
    $quarter = QuarterLog::where('location_id',$station->id)
                        ->where('quarter_start_date',$startDate)
                        ->where('quarter_end_date',$endDate)
                        ->first();
    if($quarter){
      $commission = $quarter->amount;
    }
    return round($commission,2);
  }

  private function commissionCalculator($station,$startDate,$endDate)
  {
    $sales = $station->sales->whereBetween('item_date', [$startDate, $endDate])
      ->where('item_date','<>',$endDate);
    $commission  = 0;
    if(!$sales->isEmpty()){
      $cash = $sales->sum('cash');
      $credit = $sales->sum('credit');

      $commission =  round(((floatval($cash) + floatval($credit)) *  floatval($station->commission_percent)) / 100,2);
    }
    return $commission;
  }


}
