<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Nova\User as NovaUser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class UserPermissionController extends Controller
{

     // Fetch all users
     public function getUsers()
     {
         $users = User::where('role_id',User::ROLE_ADMIN)->with('permissions')->get();
         return response()->json($users);
     }
 
     // Fetch all permissions
     public function getPermissions()
     {
         $permissions = Permission::all();
         return response()->json($permissions);
     }
 
     // Assign permissions to a user
     public function assignPermissions(Request $request)
     {
         $request->validate([
             'user' => 'array|required',
             'user.id' => 'required|exists:users,id',
             'permissions' => 'array',
             'permissions.*.id' => 'required|exists:permissions,id', // Validate the 'id' inside each permission object
             'permissions.*.name' => 'required|exists:permissions,name', // Validate the 'id' inside each permission object

         ]);
    
         $user = User::where('id',$request->user['id'])->where('role_id',User::ROLE_ADMIN)->first();

         // If user is not found, return a 422 error response
        if (!$user) {
            return response()->json([
                'message' => 'User not found or not an admin',
            ], 422);
        }

         $permissionIds = collect($request->permissions)->pluck('id')->toArray();
     
         $user->permissions()->sync($permissionIds);

         return response()->json(['message' => 'Permissions assigned successfully']);
     }

}
