<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\InviteEmail;
use App\Mail\SupportEmail;
use App\Models\MemberLocation;
use App\Models\Station;
use App\Models\TeamMember;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
          'name' => 'required|string',
          'password' => 'nullable',
        ]);
        $user = \auth()->user();

        $user->name = $request->name;
        if(trim($request->password))
        {
          $user->password = Hash::make($request->password);
        }
        $user->save();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendInvite(Request $request, $clientUuid)
    {
      $request->validate([
        'email' => 'required|email|unique:team_members,invite_email|unique:users,email',
        'locations.*' => 'array',
        'locations.*.id' => 'required|integer',
      ]);

      $user = Auth::user();
      if( $user->uuid ==  $clientUuid ) {
        $clientId = $user->id;
      }elseif(  $user->hasAnyRole('owner','admin') )
      {
        $clientId = optional(User::where('uuid',$clientUuid)->firstOrFail())->id;
      }else{
        abort(401);
      }
      $code = Str::orderedUuid();
      $member = TeamMember::create([
                                    'client_id' => $clientId,
                                    'invite_email' => $email = $request->email,
                                    'invite_code' => $code,
                                  ]);
      $locations = [];
      foreach ($request->locations as $location){
        MemberLocation::create(['station_id' => $location['id'] , 'team_member_id' => $member->id]);
      }

      Mail::to($email)->send(new InviteEmail($user,$code));

      return $member;
    }

  public function updateMemberLocation(Request $request, $clientUuid,$memberId)
  {
    $request->validate([
      'locations.*' => 'array',
      'locations.*.id' => 'required|integer',
    ]);

    $user = Auth::user();
    if( $user->uuid ==  $clientUuid ) {
      $clientId = $user->id;
    }elseif(  $user->hasAnyRole('owner','admin') )
    {
      $clientId = optional(User::where('uuid',$clientUuid)->firstOrFail())->id;
    }else{
      abort(401);
    }

    $data = [];

    $member = TeamMember::findOrFail($memberId);

    foreach ($request->locations as $location){
       array_push($data,$location['id']);
    }

    $member->locations()->sync($data);
    return $member;
  }


  public function acceptInvite(Request $request,$inviteCode)
    {
      $member = TeamMember::where('invite_code',$inviteCode)
                          ->where('accpeted_at',null)
                          ->firstOrfail();

      $client = User::findOrFail($member->client_id);

      $request->merge(["email"=> $member->invite_email]);

      $request->validate([
        'name' => 'required|string',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|confirmed',
      ]);

      $uuid = Str::orderedUuid();

      $user = User::create([
                            'uuid' => $uuid,
                            'name' => $request->name,
                            'email' => $request->email,
                            'password' => Hash::make($request->password),
                            'client_uuid' => $client->uuid,
                            'role_id' => User::ROLE_SUB_CLIENT,
                            'is_active' => 1,
                          ]);
      $user->assignRole(User::ROLE_SUB_CLIENT);
      $member->member_id = $user->id;
      $member->accpeted_at = now();
      $member->save();

      MemberLocation::where('team_member_id',$member->id)->update(['user_id'=>$user->id]);

      return [];
    }


    public function getTeam($clientUuid)
    {
      $user = Auth::user();
      if( $user->client_uuid != $clientUuid && $user->hasAnyRole('owner','admin') ) {
        $clientId = optional(User::where('uuid',$clientUuid)->first())->id;
      }elseif( $user->uuid == $clientUuid )
      {
        $clientId = optional(User::where('uuid',$user->client_uuid)->first())->id;
      }
      else{
        abort(401);
      }

      $members = TeamMember::where('client_id',$clientId)->with('user','locations')->get()->makeHidden('pivot');

      return $members;
    }


  public function deleteMember($clientUuid,$teamId)
  {
    $user = Auth::user();

    if( $user->client_uuid != $clientUuid && $user->hasAnyRole('owner','admin') ) {
      $clientId = optional(User::where('uuid',$clientUuid)->first())->id;
    }elseif( $user->uuid == $clientUuid )
    {
      $clientId = optional(User::where('uuid',$user->client_uuid)->first())->id;
    }
    else{
      abort(401);
    }
    $member = TeamMember::where('client_id',$clientId)
                          ->where('id',$teamId)
                          ->firstOrFail();
    $deleteUser = User::find($member->member_id);

    if($deleteUser){
      $deleteUser->delete();
    }
    MemberLocation::where('team_member_id',$member->member_id)->delete();
    $member->delete();
    return [];
  }
}
