<?php

namespace App\Http\Services;

use GuzzleHttp\Exception\ClientException;

class SheetDBService
{
  private $sheetDBAPI;
  private $client;

  public function __construct()
  {
    $this->sheetDBAPI = config('app.sheet_db_key_uri');
    $this->client = new \GuzzleHttp\Client(['headers' => [ 'Content-Type' => 'application/json' ]]);
  }

  public function createRows($data)
  {
    $response = $this->client->post($this->sheetDBAPI,
      ['body' => json_encode(
        [
          'data' => json_encode($data),
          'sheet' => 'Locations'
        ]
      )]
    );

    if($response->getStatusCode() == 201){
      return true;
    }
    report($response);
 }

  public function updateRows($data,$locId)
  {
    $urlSufix = '/Location ID/'.$locId;
    try{
      $response = $this->client->patch($this->sheetDBAPI.$urlSufix,
        [
          'body' => json_encode(
            [
              'data' => json_encode($data),
              'sheet' => 'Locations'
            ])
        ]
      );
      return $response->getStatusCode();

    }
    catch (ClientException $e)
    {
      if($e->getCode() != 404)
      {
        report($e);
      }
      return $e->getCode();
    }
  }

  public function deleteRow($locId)
  {
    $urlSufix = '/Location ID/'.$locId;
    try {
      $response = $this->client->delete($this->sheetDBAPI.$urlSufix,
        [
          'body' => json_encode(
            [
              'sheet' => 'Locations'
            ])
        ]
      );
    }catch (ClientException $e)
    {
      if($e->getCode() != 404)
      {
        report($e);
      }
    }
  }

  public function getRows($sheet,$limit = null,$sortBy =null ,$sortOrder= null, $sortMethod = null)
  {
    $params = ['sheet' => $sheet];

    if($limit){
      $params['limit'] = $limit;
    }
    if($sortBy)
    {
      $params['sort_by'] = $sortBy;
    }

    if($sortOrder)
    {
      $params['sort_order'] = $sortOrder;
    }

    if($sortMethod)
    {
      $params['sort_method'] = $sortMethod;
    }

    try {
       $response = $this->client->get($this->sheetDBAPI,
        [
          'body' => json_encode($params)
        ]
      );
     }catch (ClientException $e)
    {
      if($e->getCode() != 404)
      {
        report($e);
      }
    }

    if($response->getStatusCode() == 200){
      return json_decode($response->getBody());
    }
    return [];
  }

}
