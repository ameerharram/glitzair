<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\StreamedResponse;

class Redirect403
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
      $response = $next($request);

      if($response instanceof StreamedResponse)
      {
        return $response;
      }

      if ($response->status() == 403 ) {
        $user = $request->user();
        if ($user && !$user->hasAnyRole('owner ', 'admin')) {
          return redirect()->route('home', ['uuid' => $request->user()->client_uuid]);
        }
      }


      return $response;
    }
}
