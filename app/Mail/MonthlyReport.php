<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class MonthlyReport extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
      $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('emails.monthly-report')
        ->from(getenv('MAIL_FROM_ADDRESS'))
        ->to(getenv('OWNER_EMAIL'))
        ->bcc(getenv('DEV_EMAIL'))
        ->subject('Glitz Air | Monthly report '. $this->data['date'] )
        ->with($this->data);
    }

}
