<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    protected $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('emails.support')
                  ->from(getenv('MAIL_FROM_ADDRESS'))
                  ->subject($this->request->subject .' | '. $this->data['location_name'] )
                  ->with([
                    'user_id' => $this->data['user_id'],
                    'user_name' =>   $this->data['user_name'],
                    'user_email' =>   $this->data['user_email'],
                    'location_id' =>  $this->data['location_id'],
                    'location_name' =>  $this->data['location_name'],
                    'location_ownerid' =>  $this->data['location_ownerid'],
                    'location_phone' =>  $this->data['location_phone'],
                    'message' => $this->request->message,
                    'subject' => $this->request->subject,
                    'reply_to' => $this->request->email
                  ]);
    }
}
