<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$code)
    {
        $this->user = $user;
        $this->code = $code;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('emails.invite')
                  ->from(getenv('MAIL_FROM_ADDRESS'))
                  ->subject('You’ve been invited to GlitzAir.' )
                  ->with([
                    'invited_by' => $this->user->name,
                    'invite_url' => route('invite.signup',[ 'invite_code' => $this->code]) ,
                  ]);
    }
}
