<?php


namespace App\Services;


use App\Models\Machine;
use App\Models\Sale;
use App\Models\Station;
use App\Models\Transactions;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;

class MachineService
{
    protected $minCointValue = 0.25;

    /**
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function importSeelive()
    {
        $files = Storage::files(env('FILE_FOLDER', 'public/seedlive').'/seedlive');
        $now = now()->toDateString();
        //Transactions::truncate();
        foreach ($files as $file)
        {

            $transactions = (new FastExcel())->import(Storage::path($file));
            $transactions = $transactions->filter(function ($transaction) {
                $amount = floatval(trim($transaction['Amount'],'$'));
                //Handling duplicate Case
                if($transaction['Item Type'] == 'CASH' && $amount > $this->minCointValue)
                {
                    return false;
                }
                return true;
            });

            $transactions = $transactions->map(function ($transaction) use ($now){
            //For adjusting amount value
            $twoTierPrice = floatval(trim($transaction['Two-Tier Pricing'],'$'));
                return [
                    'machine_id' =>null,
                    'machine_type' => Machine::TYPE_SEEDLIVE,
                    'location_id' => null,
                    'device_number' => $transaction['Device'],
                    'ref_number' => $transaction['Item Ref #'],
                    'transaction_date' => Carbon::make($transaction['Item Date']),
                    'transaction_type' => str_contains($transaction['Item Type'],'CREDIT') ? Transactions::TYPE_CREDIT: Transactions::TYPE_CASH,
                    'amount' => floatval(trim($transaction['Amount'],'$')) - $twoTierPrice,
                    'status' => Transactions::STATUS_PROCESSABLE,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            });

            foreach (array_chunk($transactions->toArray(),1000) as $transactionData)
            {
                Transactions::upsert($transactionData,['machine_type','device_number','ref_number'],['machine_id','location_id','transaction_date','transaction_type','amount','status']);
            }
        }

        Storage::delete($files);
    }



  /**
   * @throws \Box\Spout\Common\Exception\IOException
   * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
   * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
   */
    public function importNayaxXML()
  {

    $files = Storage::files(env('FILE_FOLDER', 'public/nayax').'/nayax');

    foreach ($files as $file) {
      if(substr($file, -4) == '.zip')
      {
        $zip = new \ZipArchive();
        $fPath = Storage::path($file);
        $res = $zip->open($fPath);
        if ($res === true) {
          $zip->extractTo(Storage::path('public/nayax'));
          $zip->close();
          Storage::delete($file);
        }
      }
    }

    $files = Storage::files(env('FILE_FOLDER', 'public/nayax').'/nayax');
    $now = now();

    foreach ($files as $file)
    {
      $xmlString = Storage::get($file);
      $xmlObject = simplexml_load_string($xmlString);

      $json = json_encode($xmlObject);
      $array = json_decode($json, true);
      if($this->isAssoc($array['Table']))
      {
        $transactions = [$array['Table']];
      }else
      {
        $transactions = $array['Table'];
      }

      $transactions = collect($transactions);

      if( !$this->checkNayaxKeys($transactions->first()) ){
        continue;
      }

      $transactions = $transactions->map(function ($transaction) use ($now){
        return [
          'machine_id' =>null,
          'machine_type' => Machine::TYPE_NAYAX,
          'location_id' => null,
          'device_number' => $transaction['HW_serial'],// machine number
          'ref_number' => $transaction['transaction_id'],//Transaction ID
          'transaction_date' => Carbon::parse($transaction['auTime']),
          'transaction_type' => str_contains($transaction['payment_method_descr'],'Credit') ? Transactions::TYPE_CREDIT: Transactions::TYPE_CASH,
          'amount' => floatval(trim($transaction['auValue'],'$')),
          'status' => Transactions::STATUS_PROCESSABLE,
          'created_at' => $now,
          'updated_at' => $now
        ];
      });

      foreach (array_chunk($transactions->toArray(),1000) as $transactionData)
      {
        Transactions::upsert($transactionData,['machine_type','device_number','ref_number'],['machine_id','location_id','transaction_date','transaction_type','amount','status']);
      }

    }

    Storage::delete($files);
  }

  /**
   * @throws \Box\Spout\Common\Exception\IOException
   * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
   * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
   */
  public function importNayax()
  {
    $files = Storage::files(env('FILE_FOLDER', 'public/nayax').'/nayax');
    $now = now()->toDateString();
    //Transactions::truncate();

    foreach ($files as $file)
    {
      $transactions = (new FastExcel())->import(Storage::path($file));
      if( !$this->checkNayaxKeys($transactions->first()) ){
        continue;
      }

      $transactions = $transactions->map(function ($transaction) use ($now){
        return [
          'machine_id' =>null,
          'machine_type' => Machine::TYPE_NAYAX,
          'location_id' => null,
          'device_number' => $transaction['HW_serial'],// machine number
          'ref_number' => $transaction['transaction_id'],//Transaction ID
          'transaction_date' => Carbon::createFromFormat('d/m/Y H:i:s',$transaction['auTime']),
          'transaction_type' => str_contains($transaction['payment_method_descr'],'Credit') ? Transactions::TYPE_CREDIT: Transactions::TYPE_CASH,
          'amount' => floatval(trim($transaction['auValue'],'$')),
          'status' => Transactions::STATUS_PROCESSABLE,
          'created_at' => $now,
          'updated_at' => $now
        ];
      });

      foreach (array_chunk($transactions->toArray(),1000) as $transactionData)
      {
        Transactions::upsert($transactionData,['machine_type','device_number','ref_number'],['machine_id','location_id','transaction_date','transaction_type','amount','status']);
      }
    }

    Storage::delete($files);
  }

    /**
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function importNayaxSpecialSheet()
    {
        $files = Storage::files(env('FILE_FOLDER', 'public/nayax').'/nayax');
        $now = now();
        //Transactions::truncate();

        foreach ($files as $file)
        {
            $transactions = (new FastExcel())->import(Storage::path($file));
            $transactionsNew = collect();

            foreach ($transactions->toArray() as $index => $transaction){
              if($transaction['machine_name'] != '' && $transaction['machine_name']  != null)
              {
                $transactionsNew->push([
                  'operator_identifier' => $transactions[$index+1]['operator_identifier'],
                  'HW_serial' => $transactions[$index+1]['operator_identifier'],
                  'transaction_id' => $transactions[$index+1]['recognition_descr'],
                  'payment_method_descr' => $transactions[$index+1]['machineSeTime'],
                  'auTime' => $transaction['auTime'],
                  'auValue' => $transaction['auValue'],
                  'index' => $index
                ]);
              }else {
                continue;
              }
            }
          $transactions = $transactionsNew;
            if( !$this->checkNayaxKeys($transactions->first()) ){
                continue;
            }

            $transactions = $transactions->map(function ($transaction) use ($now){
                return [
                    'machine_id' =>null,
                    'machine_type' => Machine::TYPE_NAYAX,
                    'location_id' => null,
                    'device_number' => $transaction['HW_serial'],// machine number
                    'ref_number' => $transaction['transaction_id'],//Transaction ID
                    'transaction_date' => Carbon::createFromFormat('d/m/Y H:i:s',$transaction['auTime']),
                    'transaction_type' => str_contains($transaction['payment_method_descr'],'Credit') ? Transactions::TYPE_CREDIT: Transactions::TYPE_CASH,
                    'amount' => floatval(trim($transaction['auValue'],'$')),
                    'status' => Transactions::STATUS_PROCESSABLE,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            });

            foreach (array_chunk($transactions->toArray(),1000) as $transactionData)
            {
                Transactions::upsert($transactionData,['machine_type','device_number','ref_number'],['machine_id','location_id','transaction_date','transaction_type','amount','status']);
            }
        }

        Storage::delete($files);
    }

    public function recordMachinesDaySales()
    {
        //Turning Off Strict Mode for this Patch for group by:
        config(['database.connections.mysql.strict' => false]);
        DB::reconnect();

        $cursor = DB::table('transactions as t')
                    ->select(

                    'm.station_id',
                    't.device_number as machine_number',
                    DB::raw( 'date(t.transaction_date) as item_date'),
                    //'t.machine_type',
                    DB::raw('GROUP_CONCAT(t.id) as tIds,
                     SUM(
                     IF(TIME( t.transaction_date ) >= TIME(s.open_time) and TIME( t.transaction_date ) <= TIME(s.close_time),IF(t.transaction_type = 2,t.amount,0.00),0.00)
                     ) + IFNULL(sales.cash,0.00) as cash,
                     SUM(
                     IF(TIME( t.transaction_date ) >= TIME(s.open_time) and TIME( t.transaction_date ) <= TIME(s.close_time),IF(t.transaction_type = 1,t.amount,0.00),0.00)
                     ) + IFNULL(sales.credit,0.00) as credit,
                     SUM(
                     IF(TIME( t.transaction_date ) >= TIME(s.open_time) and TIME( t.transaction_date ) <= TIME(s.close_time),0.00,t.amount)
                     ) + IFNULL(sales.amount_after_op_hours,0.00) as amount_after_op_hours,
                     SUM(
                     IF(TIME( t.transaction_date ) >= TIME(s.open_time) and TIME( t.transaction_date ) <= TIME(s.close_time),0.00,IF(t.transaction_type = 2,t.amount,0.00))
                     ) + IFNULL(sales.cash_off_hours,0.00) as cash_off_hours,
                     SUM(
                     IF(TIME( t.transaction_date ) >= TIME(s.open_time) and TIME( t.transaction_date ) <= TIME(s.close_time),0.00,IF(t.transaction_type = 1,t.amount,0.00))
                     ) + IFNULL(sales.credit_off_hours,0.00) as credit_off_hours',
                    ),
                    //'machines.id as machine_id',
                    //'t.transaction_type as transaction_type',
                    )
                    ->join('machines as m', function ($join) {
                        $join->on('t.device_number', '=', 'm.credit') // Check for credit only machine
                               ->where('t.transaction_type',Transactions::TYPE_CREDIT)
                               ->orOn('t.device_number', '=', 'm.cash_and_credit');
                    })
                    ->join('stations  as s','s.id','=','m.station_id')
                    ->leftJoin('sales',function ($join){
                        $join->on('sales.station_id', '=', 'm.station_id')
                             ->on('sales.machine_number','=','t.device_number')
                             ->on('sales.item_date', '=', DB::raw( 'date(t.transaction_date)'));
                    })
                    ->where('t.status',Transactions::STATUS_PROCESSABLE)
                    ->where('m.status',1)
                    ->where('s.status',1)
                    ->groupByRaw('t.device_number,date(t.transaction_date)')
                    ->cursor();

        foreach ($cursor->chunk(1000) as $transactions)
        {
            $tIds = [];
            $sales = json_decode(json_encode($transactions->map(function ($item,$key) use (&$tIds){
                $tIds = array_unique(array_merge($tIds,explode(',',$item->tIds)));
                unset($item->tIds);
                if($item->machine_number != null && trim($item->machine_number) != ''){
                  return $item;
                }
            })->toArray()), true);

            Sale::upsert(
                $sales,
                [
                    'station_id',
                    'machine_number',
                    'item_date',
                ],
                [
                    'cash','credit','amount_after_op_hours','cash_off_hours','credit_off_hours'
                ]
            );

            Transactions::whereIn('id',$tIds)
                        ->update(['status' => Transactions::STATUS_PROCESSED]);

        }

      //Setting all other trasncation to non processible
      Transactions::where('status', Transactions::STATUS_PROCESSABLE )
                  ->update(['status' => Transactions::STATUS_NOT_PROCESSABLE]);

        //Turning ON Strict Mode for this Patch for group by:
        config(['database.connections.mysql.strict' => true]);
        DB::reconnect();
    }

    private function checkNayaxKeys($arr)
    {
        $nayaxKeys = [
            'operator_identifier',
            'transaction_id',
            'auTime',
            'payment_method_descr',
            'auValue'
        ];

        foreach ($nayaxKeys as $key){
            if(!array_key_exists($key,$arr)){
                return false;
            }
        }

        return true;
    }

    function isAssoc(array $arr)
    {
      if (array() === $arr) return false;
      return array_keys($arr) !== range(0, count($arr) - 1);
    }

}
