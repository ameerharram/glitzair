<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;

    const STATUS_PROCESSABLE = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_NOT_PROCESSABLE = 3;

    const TYPE_CREDIT = 1;
    const TYPE_CASH = 2;



}
