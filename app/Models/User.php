<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles,HasPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'uuid',
        'password',
        'client_uuid',
        'role_id',
        'is_active'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const OWNER_ID = 1;

    const ROLE_OWNER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_CLIENT = 3;
    const ROLE_SUB_CLIENT = 4;

    /**
     *  Accessor
     */
    public function getIsClientOwnerAttribute()
    {
        if( ($this->role_id == self::ROLE_CLIENT || $this->role_id == self::ROLE_SUB_CLIENT)  && $this->client_uuid == $this->uuid){
            return true;
        }
        return false;
    }
    /**
     * Relations
     */
    public function stations()
    {
        if($this->IsClientOwner)
        {
            return $this->hasMany(Station::class,'owner_id','id');
        }else
        {
          return $this->belongsToMany(Station::class,'member_locations','user_id','station_id');
        }
    }

    /**
     * Relations
     */
    public function locations()
    {
        return $this->hasMany(Station::class,'owner_id','id');
    }

    public function machines()
    {
        return $this->hasManyThrough(
            Machine::class,
            Station::class,
            'owner_id', // Foreign key on the Station table...
            'station_id', // Foreign key on the Machine table...
            'id', // Local key on the User table...
            'id' // Local key on the Station table...
        );
    }

    public function mlocs()
    {
       return $this->hasManyThrough(
                        MemberLocation::class,
                        TeamMember::class, //client
                        'member_id', // Foreign key on the client table...
                        'team_member_id', // Foreign key on the stations table...
                        'id', // Local key on the projects table...
                        'id' // Local key on the environments table...
                      );
    }

}
