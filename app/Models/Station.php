<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    use HasFactory;

    protected $appends = ['opening_hours','total_coins'];


    /**
     * Accessor
     */
    public function getOpeningHoursAttribute()
    {
        $open_time = substr($this->open_time, 0, -3);
        $close_time = substr($this->close_time, 0, -3);
        return "$open_time to $close_time";
    }

    public function getTotalCoinsAttribute()
    {
      $coins = $this->coins()->get();
      return $coins->sum('coins_collected');
    }

    public function getPreviousQuarterCommAttribute()
    {
        if($this->sales->isEmpty()){
            return null;
        }
        $now = now()->subQuarter(1);
        $sales = $this->sales->whereBetween('item_date', [$now->startOfQuarter()->toDateString(), $now->endOfQuarter()->toDateString()]);

        if($sales->isEmpty()){
            return null;
        }
        $cash = $sales->sum('cash');
        $credit = $sales->sum('credit');
        return round(((floatval($cash) + floatval($credit)) *  floatval($this->commission_percent)) / 100,2);
    }

    public function getThisQuarterCommAttribute()
    {

        if($this->sales->isEmpty()){
            return null;
        }
        $now = now();
        $sales = $this->sales->whereBetween('item_date', [$now->startOfQuarter()->toDateString(), $now->endOfQuarter()->toDateString()]);

        if($sales->isEmpty()){
            return null;
        }
        $cash = $sales->sum('cash');
        $credit = $sales->sum('credit');
        return round(((floatval($cash) + floatval($credit)) *  floatval($this->commission_percent)) / 100,2);
    }

    public function getThisQuarterAttribute()
    {
      if($this->sales->isEmpty()){
        return null;
      }
      $now = now();
      $sales = $this->sales->whereBetween('item_date', [$now->startOfQuarter()->toDateString(), $now->endOfQuarter()->toDateString()]);

      if($sales->isEmpty()){
        return null;
      }
      $cash = $sales->sum('cash');
      $credit = $sales->sum('credit');
      return round((floatval($cash) + floatval($credit)) ,2);
    }

    /**
     * Scopes
     */

    public function scopeActive($query)
    {
      return $query->where('status', 1);
    }

    /**
     * Relations
     */
    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id','id');
    }

    public function machines()
    {
        return $this->hasMany(Machine::class,'station_id');
    }

    public function activeMachines()
    {
        return $this->hasMany(Machine::class,'station_id')->where('status',1);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class,'station_id','id');
    }
    public function coins()
    {
      return $this->hasMany(CoinCollection::class,'location_id');
    }

}
