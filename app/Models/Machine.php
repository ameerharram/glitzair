<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    use HasFactory;

    // CONSTANST
    const TYPE_SEEDLIVE = 1;
    const TYPE_NAYAX = 2;

    const STATUS_IN_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Relationship
     */
    public function station()
    {
        return $this->belongsTo(Station::class,'station_id','id');
    }
}
