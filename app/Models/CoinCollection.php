<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinCollection extends Model
{
    use HasFactory;
    protected $table = 'coins_collection';

    public function location()
    {
      return $this->belongsTo(Station::class,'location_id');
    }
}
