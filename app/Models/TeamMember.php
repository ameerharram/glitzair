<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function locations()
    {
        return $this->belongsToMany(Station::class,'member_locations','team_member_id','station_id');
    }

    public function user()
    {
      return $this->belongsTo(User::class,'member_id');
    }

}
