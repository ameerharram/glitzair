<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    //Casts of the model dates
    protected $casts = [
        'item_date' => 'date'
    ];

    /**
     * Relationships
     */
    public function station()
    {
        return $this->belongsTo(Station::class,'station_id','id');
    }
}
