<?php

namespace App\Policies;

use App\Models\Station;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;

class StationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return  true;
        //return $user->hasAnyRole('owner','admin');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Station  $station
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Station $station)
    {
        if($user->hasAnyRole('owner','admin'))
        {
            return  true;
        }
        else
        {
            return  $user->id == $station->owner_id;
        }
        //return $user->hasAnyRole('owner','admin');
     }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
      if(request()->route()->parameter('view') == 'resources/collections')
      {
        return false;
      }
      return $user->hasAnyRole('owner','admin');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Station  $station
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Station $station)
    {
      if(Str::contains(url()->previous() , 'resources/collections'))
      {
        return false;
      }
        return $user->hasAnyRole('owner','admin');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Station  $station
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Station $station)
    {
      if(Str::contains(url()->previous() , 'resources/collections'))
      {
        return false;
      }
        return $user->hasAnyRole('owner','admin');

    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Station  $station
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Station $station)
    {
      if(Str::contains(url()->previous() , 'resources/collections'))
      {
        return false;
      }
        return $user->hasAnyRole('owner','admin');

    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Station  $station
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Station $station)
    {
      if(Str::contains(url()->previous() , 'resources/collections'))
      {
        return false;
      }
        return $user->hasAnyRole('owner','admin');

    }
}
